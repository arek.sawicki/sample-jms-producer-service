package com.acme;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

@Configuration
public class JmsMessageProducerConfiguration {

    @Bean
    public MessageProducer messageProducer(JmsTemplate jmsTemplate, AppConfiguration configuration) {
        return new JmsMessageProducer(jmsTemplate, configuration.getDestinationQueueName());
    }
}
