package com.acme;

public interface MessageProducer {
    void send(String message);
}
