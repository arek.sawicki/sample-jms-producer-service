package com.acme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

public class JmsMessageProducer implements MessageProducer {
    private static final Logger logger = LoggerFactory.getLogger(JmsMessageProducer.class);

    private final String queueName;

    private final JmsTemplate jmsTemplate;

    JmsMessageProducer(JmsTemplate jmsTemplate, String queueName) {
        this.queueName = queueName;
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void send(String message) {
        logger.info("Send message [{}] to [{}]", message, queueName);
        jmsTemplate.convertAndSend(queueName, message);
    }
}
