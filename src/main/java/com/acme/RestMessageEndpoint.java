package com.acme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestMessageEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(RestMessageEndpoint.class);

    private final MessageProducer messageProducer;

    @Autowired
    public RestMessageEndpoint(MessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public ResponseEntity<String> send(@RequestParam(name = "message", required = false) String message) {
        if (message == null) {
            logger.warn("Null message, skipping");
            return ResponseEntity.badRequest().body("Did not send, null message");
        }
        messageProducer.send(message);
        return ResponseEntity.ok("Ok");
    }
}
